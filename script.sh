#!/bin/bash

# MAINTAINER s.lazarev @mrbeast(gitlab.com/mrbeast)

####### Requirements #######
# yum install -y sharutils #
############################

logfile="/var/log/sample.log"
workdir="/tmp"

savefile="/tmp/$(date "+%b-%d-%Y")_error.log"

cd $workdir

grep -E "$(date "+%b %d")" $logfile | grep -E "\[ERROR\]" > $savefile

uuencode $savefile $(basename $savefile) | sendmail ops@example.com
